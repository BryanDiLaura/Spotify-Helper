import utilities as ut
import argparse
from enum import Enum, auto


class SearchType(Enum):
    Artist = auto(),
    Track = auto(),


def transform_to_ids(sp, list, search_type):
    list = [l.replace(" ", "+") for l in list]

    if search_type == SearchType.Artist:
        return [sp.search(q=a, type="artist", limit=1)
                ["artists"]["items"][0]["id"] for a in list]
    elif search_type == SearchType.Track:
        return [sp.search(q=t, type="track", limit=1)
                ["tracks"]["items"][0]["id"] for t in list]


def better_suggestion_query(sp, *, artists=None, genres=None, tracks=None):
    if artists is None:
        artists = []
    if genres is None:
        genres = []
    if tracks is None:
        tracks = []

    if len(artists) > 5 or len(genres) > 5 or len(tracks) > 5:
        raise RuntimeError("Only 5 seeds are allowed!")

    return sp.recommendations(
        seed_artists=transform_to_ids(sp, artists, SearchType.Artist),
        seed_tracks=transform_to_ids(sp, tracks, SearchType.Track),
        seed_genres=genres,
        limit=50
    )["tracks"]


def print_genres(sp):
    for genre in sp.recommendation_genre_seeds()["genres"]:
        print(genre)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--list_genres", action="store_true",
                        help="Print supported genres")
    parser.add_argument("--artists", nargs='*',
                        help="List of artists to use a seeds for suggestions")
    parser.add_argument("--genres", nargs='*',
                        help="List of genres to use a seeds for suggestions")
    parser.add_argument("--tracks", nargs='*',
                        help="List of tracks to use a seeds for suggestions")
    args = parser.parse_args()

    sp = ut.initialize()
    if args.list_genres:
        print_genres(sp)
    else:
        recs = better_suggestion_query(sp, artists=args.artists,
                                       genres=args.genres, tracks=args.tracks)
        desc = f"Recommendation mixup seeds: "
        if args.artists:
            desc += f"Artists:{args.artists} "
        if args.genres:
            desc += f"Genres:{args.genres} "
        playlist = ut.make_new_playlist(
            sp, playlist_name=f"[AUTO] Better reccomendations", description=desc)
        ut.add_tracks_to_playlist(sp, recs, playlist["id"])
