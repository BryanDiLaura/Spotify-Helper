from threading import Thread, get_ident
from utilities import make_new_playlist, add_tracks_to_playlist
from better_suggestions import better_suggestion_query
import datetime


class SuggestionThread(Thread):
    def __init__(self, sp, args):
        super().__init__()
        self.thread_id = get_ident()
        self.sp = sp
        self.args = args
        self.progress = 0

    def make_description(self) -> str:
        description = f"Better suggestions for:"
        if 'artists' in self.args:
            description += f" Artists={self.args['artists']}"
        if 'tracks' in self.args:
            description += f" Tracks={self.args['tracks']}"
        if 'genres' in self.args:
            description += f" Genres={self.args['genres']}."
        description += f" Auto generated on: {datetime.date.today()}"
        return description

    def run(self):
        print(f"Starting suggestions thread. Id={self.thread_id}")

        tracks = better_suggestion_query(
            self.sp,
            artists=self.args.get("artists"),
            tracks=self.args.get("tracks"),
            genres=self.args.get("genres"),
        )

        playlist = make_new_playlist(self.sp, playlist_name=self.args["playlist_name"][0],
                                     description=self.make_description())
        add_tracks_to_playlist(self.sp, tracks, playlist['id'])
        print("done!")
