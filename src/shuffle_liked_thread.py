from threading import Thread, get_ident
from utilities import make_new_playlist, add_tracks_to_playlist
import random
import datetime


class ShuffleThread(Thread):
    def __init__(self, sp, playlist_name):
        super().__init__()
        self.thread_id = get_ident()
        self.sp = sp
        self.playlist_name = playlist_name
        self.progress = 0

    def run(self):
        print(f"Starting shuffle thread for thread {self.thread_id}...")
        response = self.sp.current_user_saved_tracks()
        tracks = response["items"]
        while len(tracks) < response["total"]:
            response = self.sp.current_user_saved_tracks(
                offset=len(tracks), limit=50)
            tracks.extend(response["items"])
            self.progress = int((len(tracks) / response["total"]) * 90)

        tracks = [t["track"] for t in tracks]
        playlist = make_new_playlist(self.sp, playlist_name=self.playlist_name,
                                     description=f"Real shuffle for the people. Auto generated on: {datetime.date.today()}")
        self.progress = 95
        random.shuffle(tracks)
        self.progress = 98
        add_tracks_to_playlist(self.sp, tracks, playlist['id'])
        self.progress = 100
        print("done!")
